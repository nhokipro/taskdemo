package vn.vnpay.taskdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.vnpay.taskdemo.entity.Merchant;

import java.util.List;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long> {
    List<Merchant> findByToken(String token);

    Boolean existsByMerchantCode(String merchantCode);
}
