package vn.vnpay.taskdemo.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.vnpay.taskdemo.DTO.MerchantCreateRequest;
import vn.vnpay.taskdemo.DTO.ResponseDTO;
import vn.vnpay.taskdemo.entity.Merchant;
import vn.vnpay.taskdemo.service.MerchantService;

@RestController
@RequestMapping(value = "/api/v1/merchant")
public class MerchantController {

    private final MerchantService merchantService;

    public MerchantController(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @PostMapping()
    public ResponseEntity createMerchant(@RequestBody MerchantCreateRequest inputMerchant) {
        Merchant createdMerchant = merchantService.createMerchant(inputMerchant);
        ResponseDTO responseDTO = ResponseDTO.builder().
                code(HttpStatus.CREATED.value())
                .message("Create Success")
                .data(createdMerchant.getId())
                .build();
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }
}
