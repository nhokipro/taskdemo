package vn.vnpay.taskdemo.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MerchantCodeExistsException extends RuntimeException {
    public MerchantCodeExistsException() {
        super();
        log.info("merchant code is exist");
    }
}
