package vn.vnpay.taskdemo.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenExistsException extends RuntimeException {
    public TokenExistsException() {
        super();
        log.info("token is exists");
    }
}
