package vn.vnpay.taskdemo.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MerchantNameLengthException extends RuntimeException {
    public MerchantNameLengthException() {
        super();
        log.info("token length too long or too short");
    }
}
