package vn.vnpay.taskdemo.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MerchantCodeLengthException extends RuntimeException{
    public MerchantCodeLengthException(){
        super();
        log.info("merchant code length too long or too short");
    }
}
